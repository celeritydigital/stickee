<?php
/**
 * Stickee Server Processing Page
 * Handles order submission
 *
 */

$response = array();

$inventory = array( 
  "5000" => 0 , 
  "2000" => 0 , 
  "1000" => 0 , 
  "500"  => 0 , 
  "250"  => 0 ,
);

// process order  using users input combined with available packages or inventory
function processOrder($left , $order){

	$order = $order;
	  
	if($left <= 0){
	  return $order;
	}elseif ($left >= 5000 ) {
	  $order["5000"] += 1;
	  $left -= 5000;
	}elseif ($left >= 2000) {
	  $order["2000"] += 1;
	  $left -= 2000;
	}elseif ($left >= 1000) {
	  $order["1000"] += 1;
	  $left -= 1000;
	}elseif ($left >= 500) {
	  $order["500"] += 1;
	  $left -= 500;
	}else{
	 $order["250"] += 1;
	 // simplify function will be called here
	 if($order["250"] >= 2 ){
	 	$order["500"] += 1;
	 	$order["250"] = 0;
	 }
	 $left -= 250;
	}

 	return processOrder($left , $order);

}

// this will be used to normalise the problem that generally occurs when 251 is passed a value
function simplify(){

}

// check user is requesting via the POST method
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	
	// get user value 
	$adata = $_POST['amountOfWidget'] ?? 0 ;

	// clean user value
	$adata = trim($adata);
  	$adata = stripslashes($adata);
 	$adata = htmlspecialchars($adata);

 	// cast value to integer
	$adata = (int)$adata;
	
	// validate user data
	if($adata <= 0){

		$response = json_encode(array(
			"success" => 0 ,
			"message" => "Invalid widget amount entered.",
		));

       	echo $response;

	}else{

		$collection = processOrder($adata , $inventory );

		// calculation
		$response = json_encode(array(
			"success" => 1 ,
			"message" => "Form was processed successfully",
			"dataset" => $collection,
			"amount" => $adata,
		));

    	echo $response;
	}

}else{

	$response = json_encode(array(
		"success" => 0 ,
		"message" => "Invalid Request Method",
	));

    echo $response;

}