<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Stickee Challenge</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    
    <script src="https://use.fontawesome.com/5a172386e9.js"></script>
    <!-- Montserrat font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,400,400i,600,700,800,900" rel="stylesheet">
     <!-- Animate CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" />
    <!-- Custom styles -->
    <link href="css/styles.css" rel="stylesheet">

  </head>

  <body>

    <!-- Page Content -->
    <div class="container">

        <div id='stickee-logo' class="row">
          <div class="col-lg-12">
            <a href="/" class="logo"><img src="images/logo.png" /></a>
          </div>
        </div>

        <div id="stickee-order-request" class="row">

          <div class="col-lg-12">
            <h2 class="wow slideInRight">Purchase Widgets</h2>
            <small class="hint wow slideInLeft">Please enter the amount of widgets requested in order.</small>
            <form id="stickee-orderForm" method="POST" action="/stickee/solve.php" class="wow bounceInDown needs-validation" novalidate >
              <div class="input-group mb-3">
                <input type="number" class="form-control col-lg-5" placeholder="Amount of widgets" aria-label="Amount of widgets" name="amountOfWidget" aria-describedby="basic-addon2">
                <div class="input-group-append" required>
                  <button id="stickee-submit-request" class="btn btn-outline-secondary uppercased" type="submit">Submit <i class="fa fa-spinner orderForm_spinning"></i></button>
                </div>
              </div>
              <div class="invalid-feedback">
                Please provide a valid city.
              </div>
            </form>
          </div>

        </div>

        <div id="stickee-generate" class="row">
          <div class="col-lg-12">
            <h2 class="wow slideInRight">Generated Widget Bundle</h2>
          </div>
        </div>

        <div id="stickee-allcontent" class="row"></div>

        <div id="stickee-generate" class="row">
          <div class="col-lg-12 wow slideInRight">
            <h3 class="total-title">Total Widgets: </h3> <span class="total-value">0</span>
          </div>

          <div class="col-lg-12 wow slideInLeft">
            <button id="stickee-submit-generate" class="btn uppercased" type="button">Complete Order &nbsp; <img src="images/arrow.png" > <!-- <i class="fa fa-spinner"></i> --> </button>
          </div>
        </div>

    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.js"></script>
    <script src="js/scripts.js"></script>

  </body>

</html>
