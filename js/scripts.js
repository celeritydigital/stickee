( function( $ ) {
	console.log("Custom Scripts");

	new WOW().init();
	var allCon = $('#stickee-allcontent');
	var completeOrder = $('#stickee-submit-generate');
	var frmss = $('#stickee-orderForm');
	//var msgss = $('#orderForm_message');
	var loaderss = $('.orderForm_spinning');
	var totals = $('.total-value');

	/**
	 * Number.prototype.format(n, x)
	 * 
	 * @param integer n: length of decimal
	 * @param integer x: length of sections
	 */
	Number.prototype.format = function(n, x) {
	    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
	    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
	};

	completeOrder.click(function(){
		console.log("Complete Order");
	});


	//msgss.hide('slow');
	loaderss.hide('slow');

	frmss.submit(function (ev) {
		//loaderss.show('slow');
		console.log("here");

		$.ajax({
			type: frmss.attr('method'),
			url: frmss.attr('action'),
			data: frmss.serialize(),
			dataType: "json",
			success: function (res) {

				if(res.success==1){
					//console.log(res.success);
					//console.log(res.message);
					//console.log(res.dataset);
					//console.log(res.amount);
					allCon.empty();
					var total = 0 ;

					var order = res.dataset;

					$.each( order , function( key, value ) {
					  //console.log( key + ": " + value );

					  var package = key;
					  if(value !== 0){
					  	var overall = (key*value);
					  	var row='<div class="col-lg-4 wow bounceInUp">';
					  	row+='<div class="widget">';
				        row+='<div class="top">';
				        row+='	<h3>'+value+'x '+ package +' Widget Pack</h3>';
				        row+='</div>';
				        row+='<div class="bottom text-center">';
				        row+='	<div><img src="images/widget.png"/></div>';
				        row+='</div>';
				        row+='</div>';
			          	row+='</div>';
					  	allCon.prepend( row );
					  	total = total + overall ;
						totals.text(total.format());
					  }
					});

					//loaderss.hide();

					//frmss[0].reset();
				}else{
					console.log(res.success);
					console.log(res.message);
				}

				//msgss.show();
				//loaderss.hide();
			}
		});

		ev.preventDefault();
	});

} )( jQuery );